# Credentify skills page test data

This repository contains test data for the Credentify skills page.
The data is located in the file `test_data.toml`, each test credential
being a single [TOML dictionary][toml-dictionary]
with at least the following kinds of key--value pairs:
```toml
[[test_credentials]]
name = "credential name"
description = "credential description"
issuing_institution = "institution name or hash"
tags = [
    "quoted",
    "tags",
    "separated",
    "by",
    "commas"
]
learning_outcome_description = "description"
dependencies = [
    "list",
    "of",
    "credential names",
    "needed to",
    "complete earn this credential"
]
```
Note that the line `[[test_credentials]]` is the same for all test credentials.
It specifies that all individual credentials should be placed in the same array
for transmitting over the wire. Other key--value pairs defined in the
[Credentify meta-data standard][meta-data-standard] are also accepted besides
the ones seen here, but they are not really needed by the skills page visualizer.

**Note:**
Accepted credential tags are found in the first `Skills` column of the
[ESCO skill mapping][esco-skill-mapping]. Tags not found in this column
will be ignored and not taken into account by the visualizer.

## An example result

The test credentials will be transformed into the following format:

```json
{
  "id": "<credential hash>",
  "achievement": {
    "id": "<achievement hash>",
    "name": "Speech to text",
    "tag": [
      "write english",
      "work in teams",
      "demonstrate willingness to learn",
      "understand written english"
    ],
    "communityIds": [
      "<community achievement hash>"
    ],
    "dependentAchievementIds": [],
    "refLanguage": null,
    "altLabel": null,
    "definition": "The course covers the basic principles and methods of translating speech to text. Both human-led methods and automatic technologies are introduced, including subtitling/captioning, print interpreting, speech recognition, re-speaking.",
    "learningOutcomeDesc": "Upon successfully completing the course, the student will 1) know the basic principles of subtitling; 2) understand differences between human and machine-produced methods and subtitles; 3) have basic knowledge of how to produce subtitles.",
    "field": null,
    "eqfLevel": null,
    "nqfLevel": null,
    "creditSystem": "ECTS",
    "creditSysTitle": "ECTS",
    "creditSysDef": null,
    "creditSysValue": "1",
    "creditSysIssuer": null,
    "canConsistOfIds": [],
    "creditSysRefNum": null,
    "numCreditPoints": "1",
    "ectsCreditPoints": "1",
    "volumeOfLearning": null,
    "isPartialQual": null,
    "waysToAcquire": null,
    "eduCredType": null,
    "entryReq": null,
    "learningOutcome": null,
    "relatedOccupation": null,
    "recognition": null,
    "awardingActivity": null,
    "awardingMethod": null,
    "gradeScheme": null,
    "modeOfStudy": "Online",
    "publicKey": null,
    "assesmentMethod": null,
    "accreditation": null,
    "homePage": null,
    "landingPage": null,
    "supplDoc": null,
    "historyNote": null,
    "additionalNote": null,
    "status": null,
    "replacesId": null,
    "replacedById": null,
    "owner": null,
    "creator": null,
    "publisherId": "<publisher hash (usually = community hash)>",
    "_updatedAt": "2021-06-09T06:03:38.876Z",
    "_createdAt": "2021-06-08T23:13:46.297Z"
  },
  "communities": [
    {
      "id": "<community hash>",
      "name": "Tampere University",
      "description": "TAU conducts scientific research in technology and architecture and provides the highest education within these fields.",
      "keys": []
    }
  ],
  "profileId": "<user ID hash>",
  "stage": 4,
  "note": null,
  "grade": null,
  "awardingBodyId": null,
  "creditsAwarded": null,
  "expiryPeriod": null,
  "cheating": null,
  "wallet": "<ethereum wallet hash>",
  "actionsOrderId": null,
  "metadata": null,
  "txHash": null,
  "_createdAt": "2021-06-11T08:06:54.172Z"
}
```
Many things will be assumed in the creation, such as the institution- and/or
publisher-related data. The hashes are simulated based on the credential data
itself.

[toml-dictionary]: https://toml.io/en/v1.0.0#table
[meta-data-standard]: https://github.com/MicroCredentials/MicroHE/blob/master/meta_data_standard_draft.md
[esco-skill-mapping]: https://gitlab.com/SeSodesa/credentify-tags-to-esco-skills/-/blob/main/the_mapping.csv
